#include "Question.hpp"
#include <string>
#ifndef RACER_NODE_H
#define RACER_NODE_H

class Node {
public:
    Node(Question *question, Node *leftBranch, Node *rightBranch, bool isLeaf, std::string classification,
         float originalEntropy, int currentDepth);

    Question *question;
    Node *leftBranch;
    Node *rightBranch;
    bool isLeaf;
    string classification;
    int histogram;
    float originalEntropy;
    int currentDepth;
};

#endif //RACER_NODE_H
