
#ifndef RACER_UTIL_H
#define RACER_UTIL_H
#include <map>
using namespace std;
enum FeatureType {
    CATEGORICAL, CONTINUOUS
};

struct accuracyReport{
    float accuracy;
    std::map<std::string, int> correctlabels;
    std::map<std::string, int> incorrectlabels;
    int correct;
    int incorrect;
    int total;
};
#endif //RACER_UTIL_H