#include <iostream>
#include <iterator>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <cassert>
#include <sstream>
#include <limits>
#include <random>
#include <bits/stdc++.h>
#include "helpers.hpp"
#include "DecisionTree.hpp"
#include "Node.hpp"

using namespace std::chrono;

#define NANOSECONDS_IN_SECOND 1000000000.0

// Parse CSV file of feature types
// fileName = name of the file
// Returns vector of FeatureType, each either Continuous or Categorical
vector <FeatureType> parseFeatureTypes(string fileName) {
    ifstream fIn;
    fIn.open(fileName, ifstream::in);
    if(!fIn.is_open()){
        cerr<<"Failed to open "<<fileName<<endl;
        exit(1);
    }
    string nextString, line, word;
    vector <FeatureType> featureTypes;
    while (fIn >> line) {
        stringstream ss(line);
        while (getline(ss, word, ',')) {
            word = (string)(word.find_first_of(" ") == 0 ? word.substr(1) : word);
            if (word.compare("categorical") == 0) {
                featureTypes.push_back(CATEGORICAL);
            } else if (word.compare("continuous") == 0) {
                featureTypes.push_back(CONTINUOUS);
            } else {
                cout << "ERROR: Feature type word (\"%s\") is not "
                     << "\"continuous\" or \"categorical\" as expected" << endl;
                exit(1);
            }
        }
    }
    return featureTypes;
}

// Parse CSV file of feature names
// fileName = name of the file
// Returns vector of strings, each a name of a feature
vector <string> parseFeatures(string fileName) {
    ifstream fIn;
    fIn.open(fileName, ifstream::in);
    if(!fIn.is_open()){
        cerr<<"Failed to open "<<fileName<<endl;
        exit(1);
    }
    string nextString, line, word;
    vector <string> features;
    while (fIn >> line) {
        stringstream ss(line);
        while (getline(ss, word, ',')) {
            word = (string)(word.find_first_of(" ") == 0 ? word.substr(1) : word);
            features.push_back(word);
        }
    }
    return features;
}

// Parse CSV file of data
// fileName = name of the file
// Returns a two dimensional vector of strings, dataset divided first in columns then in rows
vector <vector<string>> parseDataToString(string dataFile) {
    ifstream fIn;
    fIn.open(dataFile, ios::in);
    if(!fIn.is_open()){
        cerr<<"Failed to open "<<dataFile<<endl;
        exit(1);
    }
    string nextString;
    int featureNumber = 0;
    int entryNumber = 0;
    vector <vector<string>> dataVector;

    vector <string> row, emptyRow;
    string line, word, temp;
    int idx = 0;
    bool missingData;
    // Get an entry
    while (getline(fIn, line) && (line.size() > 0)) {
        missingData = false;
        row.clear();
        stringstream ss(line);
        // Divide entries into features
        while (getline(ss, word, ',')) {
            if (featureNumber >= dataVector.size()) {
                dataVector.push_back(emptyRow);
            }

            // trim front space if needed
            word = ((word.find_first_of(" ") == 0) ? word.substr(1) : word);
            if (word.compare("?") == 0) {
                missingData = true;
            }
            dataVector.at(featureNumber).push_back(word);

            ++featureNumber;
        }
        if (missingData) {
            for (int fIdx = 0; fIdx < dataVector.size(); ++fIdx) {
                dataVector.at(fIdx).pop_back();
            }
        }
        featureNumber = 0;
    }

    for (int dIdx = 0; dIdx < dataVector.size() - 1; ++dIdx) {
        assert(dataVector.at(dIdx).size() == dataVector.at(dIdx + 1).size());
    }

    return dataVector;
}

// make random selections without replacement
// originalNum = total seed size, percent = selection percentage
// Returns vector of int, each entry is unique
vector<int> randomSelect_WithoutReplacement(int originalNum, float percent) {
    std::random_device r;
    std::default_random_engine e(r());
    std::uniform_real_distribution<> U(0, std::nextafter(1.0, std::numeric_limits<double>::max()));
    int N = originalNum;
    int n = originalNum * percent;
    std::vector<int> selected;
    selected.reserve(n);
    for (unsigned t = 0; t < N; t++) {
        if ((N - t) * U(e) < n - selected.size())
            selected.push_back(t);
        if (selected.size() == n)
            break;
    }

    return selected;

}

// make random selections with replacement
// originalNum = total seed size, percent = selection percentage
// Returns vector of int, entries could be repeated
vector<int> randSelectIdxWithReplacement(int originalNum, float percent) {
    srand(time(NULL));
    vector<int> selections;
    int numSelections = (int) floor(originalNum * percent);
    for (int idx = 0; idx < numSelections; ++idx) {
        selections.push_back(rand() % originalNum);
    }
    return selections;
}

//function to split training and testing data from initial dataset based on training indices
// trainingIndices = vector of training data indices, dataString = a two dimensional vector of strings, dataset divided first in columns then in rows
// Returns vector of indices to be used for testing.
vector<int> splitTrainingAndTesting(vector<int> trainingIndices, vector <vector<string>> &dataString) {

    vector<int> testIdxs;
    int trainingSize = trainingIndices.size();
    int currentTrainingIndex = trainingIndices.back();
    trainingIndices.pop_back();
    int numDataEntries = dataString.at(0).size();
    int trainingCount = 1;

    for (int dataIdx = numDataEntries - 1; dataIdx >= 0; dataIdx--) {
        if (currentTrainingIndex == dataIdx) {
            if (trainingCount < trainingSize) {
                currentTrainingIndex = trainingIndices.back();
                trainingIndices.pop_back();
                trainingCount++;
            }

        } else {
            testIdxs.push_back(dataIdx);
        }

    }

    sort(testIdxs.begin(), testIdxs.end());

    return testIdxs;

}

//function to calculate entropy for a dataset
//data = a two dimensional vector of strings, dataset divided first in columns then in rows, indices = entries to use to calculate entropy for
// Returns entropy
float calculateEntropy(vector <vector<string>> &data, vector<int> indices) {
    //map to store label counts
    std::map<std::string, int> dataCount;
    //iterate through each label in the indices
    for (int i :indices) {
        //check if label exists
        if (dataCount.count(data[data.size() - 1][i])) {
            //increase label count
            dataCount[data[data.size() - 1][i]] += 1;


        } else {
            //add label to map
            dataCount[data[data.size() - 1][i]] = 1;

        }
    }

    float entropy = 0.0;

    map<string, int>::iterator itr;
    for (itr = dataCount.begin(); itr != dataCount.end(); ++itr) {
        //calculate probability for label
        float probability = (float) itr->second / (float) indices.size();
        if (probability > 0) {
            entropy -= probability * log2(probability);
        }
    }
    return entropy;
}

//function to calculate split entropy
/////params
// leftLabelCount = map of labels and their count on the left side,
// rightLabelCount = map of labels and their count on the right side,
//leftCount = total count of left side, rightCount = total count of right side,
// Returns entropy
float
calculateSplitEntropy(std::map<std::string, int> leftLabelCount, std::map<std::string, int> rightLabelCount,
                      int leftCount,
                      int rightCount) {
    float totalData = leftCount + rightCount;
    float probabilityRight = rightCount / totalData;
    float probabilityLeft = leftCount / totalData;

    float leftEntropy = 0.0;
    float rightEntropy = 0.0;

    map<string, int>::iterator leftitr;
    for (leftitr = leftLabelCount.begin(); leftitr != leftLabelCount.end(); ++leftitr) {
        float probability = (float) leftitr->second / (float) leftCount;
        if (probability > 0) {
            leftEntropy -= probability * log2(probability);
        }
    }

    map<string, int>::iterator rightitr;
    for (rightitr = rightLabelCount.begin(); rightitr != rightLabelCount.end(); ++rightitr) {
        float probability = (float) rightitr->second / (float) rightCount;
        if (probability > 0) {
            rightEntropy -= probability * log2(probability);
        }
    }

    //calculate split entropy by calculating entropy of both left and right data
    float splitEntropy = (probabilityLeft * leftEntropy) + (probabilityRight * rightEntropy);

    return splitEntropy;
}

//function to calculate split entropy
// featsplitData = left side and right side indices, data = a two dimensional vector of strings, dataset divided first in columns then in rows
// Returns entropy
float calculateSplitEntropy(FeatureSplitDataIndx featsplitData, vector <vector<string>> &data) {
    vector<int> splitDataLeft = featsplitData.leftSideData;
    vector<int> splitDataRight = featsplitData.rightSideData;
    float totalData = splitDataLeft.size() + splitDataRight.size();
    float probabilityDataTrue = (float) splitDataLeft.size() / totalData;
    float probabilityDataFalse = (float) splitDataRight.size() / totalData;
    float splitEntropy = (probabilityDataTrue * calculateEntropy(data, splitDataLeft)) +
                         (probabilityDataFalse * calculateEntropy(data, splitDataRight));

    return splitEntropy;
}


//function to split data to left and right (true and false)
/////params
//data = a two dimensional vector of strings, dataset divided first in columns then in rows
//splitFeature = feature index to split upon
//featureTypes = vector of each feature's type, either Continuous or Categorical
//splitValue = value to split upon
//nodeDatasetIndices = indices of data entries to use
// Returns data split into left and right side
FeatureSplitDataIndx
splitData(vector <vector<string>> &data, int splitFeature, vector <FeatureType> featureTypes, string splitValue,
          vector<int> &nodeDatasetIndices) {
    FeatureSplitDataIndx featSplitData;
    vector<int> leftSideData;
    vector<int> rightSideData;
    if (featureTypes.at(splitFeature) == CATEGORICAL) {
        for (int i: nodeDatasetIndices) {
            if (data.at(splitFeature).at(i) == splitValue) {
                leftSideData.push_back(i);
            } else {
                rightSideData.push_back(i);
            }

        }
    } else {
        for (int i: nodeDatasetIndices) {
            if (stod(data.at(splitFeature).at(i)) <= stod(splitValue)) {
                leftSideData.push_back(i);
            } else {
                rightSideData.push_back(i);
            }
        }


    }


    featSplitData = {leftSideData, rightSideData};

    return featSplitData;
}

//function to sort dataset with a certain feature (column)
/////params
//featIdx = feature index to sort on
//data = a two dimensional vector of strings, dataset divided first in columns then in rows
//featureData = saves the sorted column paired with its previous position in the dataset
//nodeDatasetIndices = indices of data entries to use
void
sortDataByFeature(int featIdx, vector <vector<string>> &data, vector <pair<int, string>> &featureData,
                  vector<int> &nodeDatasetIndices) {
    for (int dataIdx = 0; dataIdx < nodeDatasetIndices.size(); dataIdx++) {
        featureData.emplace_back(nodeDatasetIndices[dataIdx], data[featIdx].at(nodeDatasetIndices[dataIdx]));
    }
    sort(featureData.begin(), featureData.end(), [](pair<int, string> &a, pair<int, string> &b) {
        return a.second < b.second;
    });
}

//function to find best split point from possible splits
/////params
//parentEntropy = entropy at the parent node (0 if its the root node)
//currentDepth = current depth of node
//data = a two dimensional vector of strings, dataset divided first in columns then in rows
//featureTypes = vector of each feature's type, either Continuous or Categorical
//featureWeight = feature selection weigh for random feature selection
//nodeDatasetIndices = indices of data entries to use
// Returns BestSplitPoint, best split feature index and best split feature value
BestSplitPoint findBestSplit(float parentEntropy, int currentDepth, vector <vector<string>> &data,
                             vector <FeatureType> featureTypes, float featureWeight, vector<int> &nodeDatasetIndices) {

    vector<int> randomFeatures = randomSelect_WithoutReplacement(data.size() - 1, featureWeight);
    int bestFeatureIndex = randomFeatures[0];
    //start with maximum entropy
    float minEntropy = 99999;

    string bestSplitValue = "";

    //iterate through all the random features
    for (auto featureIndex: randomFeatures) {
        if (featureIndex != data.size() - 1) {//because last column is label
            if (featureTypes.at(featureIndex) == CONTINUOUS) {
                //initialize variables
                string threshold = "";
                int dataIndex;
                std::map<std::string, int> leftLabelCount;
                std::map<std::string, int> rightLabelCount;
                //count right side labels
                for (int i : nodeDatasetIndices) {
                    if (rightLabelCount.count(data[data.size() - 1][i])) {
                        rightLabelCount[data[data.size() - 1][i]] += 1;


                    } else {
                        rightLabelCount[data[data.size() - 1][i]] = 1;

                    }
                }

                int leftSize = 0;
                int rightSize = nodeDatasetIndices.size();
                vector <pair<int, string>> featureData;
                featureData.reserve(nodeDatasetIndices.size());
                //done initializing variables


                //sort data with selected feature

                sortDataByFeature(featureIndex, data, featureData, nodeDatasetIndices);

                for (int indx = 0; indx < featureData.size();) {
                    threshold = featureData.at(indx).second;
                    dataIndex = featureData.at(indx).first;

                    while (indx < featureData.size() && featureData.at(indx).second <= threshold) {
                        leftSize++;
                        rightSize--;
                        if (leftLabelCount.count(data[data.size() - 1][featureData.at(indx).first])) {
                            leftLabelCount[data[data.size() - 1][featureData.at(indx).first]] += 1;
                        } else {
                            leftLabelCount[data[data.size() - 1][featureData.at(indx).first]] = 1;
                        }
                        rightLabelCount[data[data.size() - 1][featureData.at(indx).first]] -= 1;

                        indx++;
                        if (indx < featureData.size()) {
                            dataIndex = featureData[indx].first;
                        }

                    }
                    if (indx == featureData.size()) {
                        continue;
                    }
                    float splitEntropy = calculateSplitEntropy(leftLabelCount, rightLabelCount, leftSize, rightSize);

                    if (splitEntropy < minEntropy) {
//                    cout<<"Best split at "<< featureIndex <<" value "<<threshold<<" Entropy "<< splitEntropy<<endl;
                        minEntropy = splitEntropy;
                        bestFeatureIndex = featureIndex;
                        bestSplitValue = threshold;
                    }


                }
            } else {
                //collect unique values for each categorical feature
                set <string> uniqueValues;
                for (int i: nodeDatasetIndices) {
                    uniqueValues.insert(data[featureIndex][i]);
                }

                set<string>::iterator splitItr;
                for (splitItr = uniqueValues.begin(); splitItr != uniqueValues.end(); splitItr++) {
                    FeatureSplitDataIndx featSplitData = splitData(data, featureIndex, featureTypes, (*splitItr),
                                                                   nodeDatasetIndices);
                    float splitEntropy = (float) calculateSplitEntropy(featSplitData, data);
                    if (splitEntropy < minEntropy) {

                        minEntropy = splitEntropy;
                        bestFeatureIndex = featureIndex;
                        bestSplitValue = (*splitItr);
                    }


                }

            }
        }
    }
    //check if parent entropy is less than entropy
    if (minEntropy >= parentEntropy && currentDepth != 0) {
        bestFeatureIndex = -1;
        bestSplitValue = "";
    }
    return {bestFeatureIndex, bestSplitValue};
}

//function to classify data and return entropy
/////params
//data = a two dimensional vector of strings, dataset divided first in columns then in rows
//indices = indices of data entries to use
// Returns both classification and entropy of data entries
std::pair<string, float> classifyWithEntropy(vector <vector<string>> &data, vector<int> &indices) {
    auto start = high_resolution_clock::now();
    std::map<std::string, int> dataCount;
    float entropy = 0.0;
    int maxVote = 0;
    string label;
    //iterate through all data entries in the node
    for (int i: indices) {
        //count labels
        if (dataCount.count(data[data.size() - 1][i])) {
            dataCount[data[data.size() - 1][i]] += 1;
        } else {
            dataCount[data[data.size() - 1][i]] = 1;

        }
    }
    map<string, int>::iterator itr;
    for (itr = dataCount.begin(); itr != dataCount.end(); ++itr) {
        float probability = (float) itr->second / (float) indices.size();

        if (probability > 0) {
            entropy -= (probability) * log2(probability);
        }

        if (maxVote < itr->second) {
            maxVote = itr->second;
            label = itr->first;
        }
    }


    std::pair<string, float> classificationWithEntropy(label, entropy);
    return classificationWithEntropy;

}

//function to bootstrap data
/////params
//indices = indices of data entries to use
//baggingWeight = feature selection weigh for random data entries selection
// Returns vector of data entry indices
vector<int> bootstrapData(vector<int> &indices, float baggingWeight) {
    vector<int> newData;

    //randomly select index with replacement from total data entries based on bagging weight
    vector<int> selection = randSelectIdxWithReplacement(indices.size(), baggingWeight);
    sort(selection.begin(), selection.end());
    for (int i = 0; i < selection.size(); i++) {
        newData.push_back(indices.at(selection[i]));
    }

    return newData;
}

//function to oversample data
/////params
//data = a two dimensional vector of strings, dataset divided first in columns then in rows
//indices = indices of data entries to use
// Returns vector of data entry indices
vector<int> oversample(vector <vector<string>> &data, vector<int> &indices) {
    int labelIdx = data.size() - 1;

    vector<int> oversampled;
    vector <string> emptyVecString;
    vector<int> toAdd;
    int highestCount = 0;
    map<string, int> labelCount;
    map <string, vector<int>> labelWithIdx;

    for (int i : indices) {
        //map to count labels
        if (labelCount.count(data[labelIdx][i])) {
            labelCount[data[labelIdx][i]] += 1;
            labelWithIdx[data[labelIdx][i]].push_back(i);
            if (labelCount[data[labelIdx][i]] > highestCount) {
                highestCount = labelCount[data[labelIdx][i]];
            }


        } else {
            labelCount[data[labelIdx][i]] = 1;
            labelWithIdx[data[labelIdx][i]].push_back(i);

            if (labelCount[data[labelIdx][i]] > highestCount) {
                highestCount = labelCount[data[labelIdx][i]];
            }

        }
    }

    map<string, int>::iterator countItr;
    map <string, vector<int>> idxItr;

    //iterate through each label count
    for (countItr = labelCount.begin(); countItr != labelCount.end(); countItr++) {
        string label = countItr->first;
        //calculate label count difference from highest label
        int difference = highestCount - countItr->second;
        cout << "label " << label << " is " << difference << " times less than highest label \n";
        //if there is difference
        if (difference > 0) {
            //find current label size
            int labelSize = labelWithIdx[label].size();
            //get current label index
            vector<int> idxs = labelWithIdx[label];
            //check if difference is less than label count
            while (difference > 0) {
                if (difference < labelSize) {
                    vector<int> selection = randSelectIdxWithReplacement(idxs.size(),
                                                                         (float(difference) / float(labelSize)));
                    for (int selectionIdx:selection) {
                        toAdd.push_back(idxs.at(selectionIdx));
                    }
                    difference = 0;
                } else {
                    vector<int> selection = randSelectIdxWithReplacement(idxs.size(), 1);
                    for (int selectionIdx:selection) {
                        toAdd.push_back(idxs.at(selectionIdx));
                    }
                    difference -= labelSize;

                }

            }

        }
    }
    cout << "labels to add " << toAdd.size() << endl;

    return toAdd;


}

//recursively go through the nodes and free memory
//node = pointer to root node of a tree
void cleanTree(Node *node) {
    if (node->isLeaf) {
        delete node->question;
        delete node;
        return;
    }

    cleanTree(node->leftBranch);
    cleanTree(node->rightBranch);
    delete node->question;
    delete node;
}
