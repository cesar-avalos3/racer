#include <string>
#include "util.hpp"

using namespace std;
#ifndef RACER_QUESTION_H
#define RACER_QUESTION_H
class Question{
public:
    Question(int splitFeatureIndex, string splitValue, FeatureType splitFeatureType);
    int splitFeatureIndex;
    string splitValue;
    FeatureType splitFeatureType;
};
#endif //RACER_QUESTION_H
