#include <string>
#include <vector>
#include <map>
#include <iterator>
#include <bits/stdc++.h>
#include "util.hpp"
#include "Node.hpp"

#ifndef HELPERS_HPP
#define HELPERS_HPP



using namespace std;


struct FeatureSplitDataIndx {
    vector<int> leftSideData;
    vector<int> rightSideData;
};

struct BestSplitPoint {
    int featureIdx;
    string splitpoint;
};

vector<vector<string>> parseDataToString(string dataFile);
vector <FeatureType> parseFeatureTypes(string fileName);
vector <string> parseFeatures(string fileName);
vector<int> randSelectIdxWithoutReplacement(int originalNum, float percent);
vector<int> randSelectIdxWithReplacement(int originalNum, float percent);
vector<int> splitTrainingAndTesting(vector<int> trainingIndices, vector <vector<string>> &dataString);
std::pair<string,float> classifyWithEntropy(vector<vector<string>> &data, vector<int> &indices);
FeatureSplitDataIndx splitData(vector<vector<string>>& data, int splitFeature,vector<FeatureType> featureTypes, string splitValue, vector<int>  &nodeDatasetIndices );
float calculateEntropy(vector <vector<string>>& data, vector<int> indices) ;
float calculateSplitEntropy(std::map<std::string, int> leftLabelCount, std::map<std::string, int> rightLabelCount, int leftCount,int rightCount);
float calculateSplitEntropy (FeatureSplitDataIndx featsplitData, vector<vector<string>> &data);
vector <int> bootstrapData(vector <int> &indices, float baggingWeight);
vector<int> randomSelect_WithoutReplacement(int originalNum, float percentTraining);
vector<int> oversample(vector<vector<string>> &data, vector<int> &indices);
BestSplitPoint findBestSplit(float parentEntropy, int currentDepth, vector <vector<string>> &data,
                             vector <FeatureType> featureType, float featureWeight, vector<int>&  nodeDatasetIndices );
void cleanTree(Node *node);

#endif
