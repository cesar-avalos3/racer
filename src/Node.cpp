#include "Node.hpp"
#include <string>
#include "Question.hpp"
using namespace std;
Node::Node(Question *question, Node *leftBranch, Node *rightBranch, bool isLeaf, string classification,
           float originalEntropy, int currentDepth) {
    this->question = question;
    this->leftBranch = leftBranch;
    this->rightBranch = rightBranch;
    this->isLeaf = isLeaf;
    this->classification = classification;
    this->originalEntropy = originalEntropy;
    this->currentDepth = currentDepth;
    this->histogram = 0;
}